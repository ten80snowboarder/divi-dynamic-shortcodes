<?php
// Shortcode to show the number_counter module
add_shortcode('divi_number_counter', 'show_divi_number_counter');
function show_divi_number_counter( $atts ){

	// available attributes
	$title = ( isset( $atts['title'] ) ? $atts['title'] : 'Module Title' );
	$number = ( isset( $atts['number'] ) ? $atts['number'] : '123' );
	$color = ( isset( $atts['color'] ) ? $atts['color'] : 'black' );
	$percent = ( isset( $atts['percent'] ) ? 'on' : 'off' );

	return do_shortcode('[et_pb_number_counter title="'.$title.'" number="'.$number.'" percent_sign="'.$percent.'" number_text_color="'.$color.'" /]');

}	// end show_divi_number_counter()
?>